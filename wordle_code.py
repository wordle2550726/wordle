import requests as rq
import random
DEBUG = False

class WordleBot :
    words = [words.strip() for words in open("words.txt")]
    wordle_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = wordle_url + "register"
    creat_url = wordle_url + "create"
    guess_url = wordle_url + "guess"

    def __init__(self , name: str) :
        
        self.session = rq.session()
        register_dict = {"mode" : "wordle" ,"name" : name}
        reg_resp = self.session.post(WordleBot.register_url, json = register_dict)
        self.me = reg_resp.json()['id']
        creat_dict = {"id" : self.me, "overwrite" : True}
        self.session.post(WordleBot.creat_url, json = creat_dict)
        
        self.choices = [w for w in WordleBot.words[:] ]
        random.shuffle(self.choices)

    def play(self) -> str :
        def post(choice : str) -> tuple :
            guess = {"id" : self.me, "guess" : choice}
            response = self.session.post(WordleBot.guess_url, json = guess)
            rj = response.json()
            right = rj["feedback"]
            status = "win" in rj["message"]
            return right, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        right, won = post(choice)
        tries = [f'{choice}:{right}']

        for chance in range(6) :
            if DEBUG:
                print(choice, right, self.choices[:10])
            self.update(choice, right)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            right, won = post(choice)
            tries.append(f'{choice}:{right}')

        if won :
            print("secret is", choice, "found in", len(tries), "attempts")
            print("Route is:", "=> ".join(tries))
        else :
            print("You lost the game.")
    
    def update(self, choice: str, right: str):
        positions = {}
        right_letters = []
        for pos,_ in enumerate(right, start=0) :
            if _ == "G" :
                positions[choice[pos]] = pos
            elif _ == "Y" :
                right_letters.append(choice[pos])
        
        def common(choice : str , word: str) :
            position_check = all(word[index] == letter for index, letter in positions.items())
            letter_check = len(set(choice) & set(word)) == len(right_letters)
            return position_check and letter_check
        self.choices = [w for w in self.choices if common(choice,w)]

game = WordleBot("sara")
game.play()
    




